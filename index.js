/*****************************************************************************
 * 
 MIT License
 
 Copyright (c) 2019 Simon Egli, ICF Church
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
*/
const fs = require('fs');
var path = require('path');
const csv = require('csvtojson'); 

const fileOutput = "./input/songs.json";

// 1. Search for an input file in ./input. 
console.log('Searching for input file...');
var inputDir = fs.readdirSync('./input/');
var csvFilePath = null;

for(var i in inputDir) {
  if(path.extname(inputDir[i]) === ".csv") {
    csvFilePath = './input/' + inputDir[i];
    break;
  }
}

// 2. Check if input file exists
if (!csvFilePath) {
  console.error('Could not find report file (*.csv) in ./input folder...');
  console.log('Please download a CCLI report from elvanto in CSV format and place it here: ./input');
  process.exit();
}

console.log('File found: ' + csvFilePath);

// 3. Read input file and convert to .json.
csv()
.fromFile(csvFilePath)
.then((jsonObj) => {

  var jsonContent = JSON.stringify(jsonObj);
    
  fs.writeFile(fileOutput, jsonContent, 'utf8', function (err) {
    if (err) {
      console.log("An error occured while writing JSON Object to File.");
      return console.log(err);
    }

    console.log("Convert .csv to .json... done!");
  });
});