# Description
This is a simple utility tool to auto-report songs to CCLI. It takes an CCLI report from an Elvanto ([www.elvanto.com](https://www.elvanto.com)) account as input and automates the reporting.


# Requirements
- Only works on Mac OSX.  
- CCLI report (*.csv) input file only works with German column names. The columns MUST be the following:    
"Lied","Status","CCLI Nummer","Mal genutzt","Drucken","Digital","Übersetzung","Aufnahme","Datei downloaden"  
- Make sure your songs in Elvanto have a valid CCLI number.

>**Workaround:**  
If your Elvanto account is set to a different language then German, you can rename the column names manually before running `yarn run automate`.


# Installation
**Step 1:**
Go to https://nodejs.org/en/download/, download and install Node.

**Step 2:**  
Install yarn package manager:  
```  
curl -o- -L https://yarnpkg.com/install.sh | bash
```
> Important: If this fails please create the file ".profile" in your home directory.

**Step 3:**  
Download the following ZIP file and extract it:  
https://gitlab.com/icf-church/side-projects/ccli-auto-reporting/-/archive/master/ccli-auto-reporting-master.zip  


# Configuration
Copy and rename ***config.json.tmp*** to ***config.json*** and enter your CCLI credentials. The file should look something like this in the end:  

```
{
  "fixturesFolder": "./input",
  "env": {
      "ccli_username": "email@domain.com",
      "ccli_password": "*********"
  }
}
```


# Usage
**Step 1:**  
Download a CCLI report from your Elvanto account. Make sure you choose CSV as the output format.  

**Step 2:**  
Copy the file to ***./input***. The tool only reads the first .csv file in the folder.  

**Step 3:**  
Start the automation:  
```
./start-reporting.sh
```
