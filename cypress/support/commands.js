// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
Cypress.Commands.add('login', (email, password) => {

    cy.log('Login to CCLI website...');

    cy.visit('https://olr.ccli.com/Account/Logout');
    //cy.visit('https://olr.ccli.com/');
    
    //cy.contains('Sign in with your CCLI Profile');
    cy.url().should('include', '/account/signin');
    
    cy.get('#EmailAddress')
    .type(email)
    .should('have.value', email);

    cy.get('#Password')
    .type(password)
    .should('have.value', password);

    cy.get('#sign-in').click();
    cy.log('Login completed!');
});

//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
