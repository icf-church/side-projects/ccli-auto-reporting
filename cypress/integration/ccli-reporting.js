/*****************************************************************************
 * 
 * MIT License
 * 
 * Copyright (c) 2019 Simon Egli, ICF Church
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

describe('CCLI Autofill', function() {
  before(function () {

    // Login into ccli website before reporting songs.
    const email = Cypress.env('ccli_username');
    const password = Cypress.env('ccli_password');
    cy.login(email, password);
  })

  beforeEach(function () {
    Cypress.Cookies.preserveOnce('ASP.NET_SessionId', 'CCLI_APPUSERCONTEXT', 'CCLI_AUTH', 'CCLI_JWT_AUTH', '__RequestVerificationToken');
  })

  // Open the website and check logged in status.
  it('Open CCLI Website', function() {
    cy.visit('https://olr.ccli.com/');
    cy.url().should('eq', 'https://olr.ccli.com/');
  });

  //it('Change Language', function() {
  //  cy.get('#culturePreference').select('de-DE');
  //  cy.contains('Liedsuche nach Titel, Autor, Liedtext, Liednummer oder Rechteinhaber');
  //});

  // Search each song and report
  it('Search Song', function() {

    // Load songs from ./input/songs.json
    cy.fixture('songs.json').as('songs');

    const canReportAtOnce = 9;

    cy.log('Load songs from songs.json');
    cy.log('Start reporting songs.');

    cy.get('@songs').then((songs) => {

      
      // Loop songs
      songs.forEach(song => {
        if (song["CCLI Nummer"] !== '') {

          cy.log('Song CCLI : ' + song["CCLI Nummer"]);
          cy.log('Song Name: ' + song.Lied);

          let print = song.Drucken;
          let digital = song.Digital;
          let translation = song["Übersetzung"];

          while (print % canReportAtOnce > 0 || digital % canReportAtOnce > 0 || translation % canReportAtOnce > 0) {

            cy.get('#SearchTerm').clear().type(song["CCLI Nummer"]).should('have.value', song["CCLI Nummer"]);
            cy.get('#frmSearchTerm').submit();

            cy.contains('1 Suchergebnisse zu');
            cy.contains('CCLI-Liednummer: ' + song["CCLI Nummer"]);
    
            cy.get('.reportAction').click();
    
            if (print > canReportAtOnce) {
              cy.get('input[name="PrintCount"]').clear().type(canReportAtOnce);//.should('have.value', canReportAtOnce);
              print -= canReportAtOnce;
            } else {
              cy.get('input[name="PrintCount"]').clear().type(print);//.should('have.value', print);
              print = 0;
            }
        
            if (digital > canReportAtOnce) {
              cy.get('input[name="DigitalCount"]').clear().type(canReportAtOnce);//.should('have.value', canReportAtOnce);
              digital -= canReportAtOnce;
            } else {
              cy.get('input[name="DigitalCount"]').clear().type(digital);//.should('have.value', digital);
              digital = 0;
            }
        
            if (translation > canReportAtOnce) {
              cy.get('input[name="TranslationCount"]').clear().type(canReportAtOnce);//.should('have.value', canReportAtOnce);
              translation -= canReportAtOnce;
            } else {
              cy.get('input[name="TranslationCount"]').clear().type(translation);//.should('have.value', translation);
              translation = 0;
            }

            cy.get('.add-activity').submit();
            cy.wait(1500);
          }

        }
        
      });
      
    });

  });
 

});


